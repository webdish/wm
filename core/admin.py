from django.contrib import admin
from .models import *


class CategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'is_visible',)
    list_editable = ('is_visible',)

admin.site.register(Category, CategoryAdmin)


class ProductsAdmin(admin.ModelAdmin):

    list_display = ('name', 'category', 'price', 'quantity', 'is_visible',)
    list_editable = ('is_visible',)

admin.site.register(Products, ProductsAdmin)


class WalletSystemAdmin(admin.ModelAdmin):

    list_display = ('rub1', 'rub2', 'rub5', 'rub10', 'total',)

    def has_add_permission(self, request):
        if WalletSystem.objects.count():
            return False
        return True

admin.site.register(WalletSystem, WalletSystemAdmin)


class TransactionAdmin(admin.ModelAdmin):

    list_display = ('rub1', 'rub2', 'rub5', 'rub10', 'total', 'created',)

admin.site.register(Transaction, TransactionAdmin)