from django.core.management.base import BaseCommand
from django.db import transaction

from core.models import *

class Command(BaseCommand):

    def handle(self, **options):

        total = 0
        count_rub1 = 0
        count_rub2 = 0
        count_rub5 = 0
        count_rub10 = 0
        objects = Transaction.objects.all()

        for object in objects:
            count_rub1 = count_rub1+object.rub1
            count_rub2 = count_rub2+object.rub2
            count_rub5 = count_rub5+object.rub5
            count_rub10 = count_rub10+object.rub10
            total = total+object.total

        wm = WalletSystem.objects.first()
        wm.rub1 = wm.rub1+count_rub1
        wm.rub2 = wm.rub2+count_rub2
        wm.rub5 = wm.rub5+count_rub5
        wm.rub10 = wm.rub10+count_rub10
        wm.total = wm.total+total
        wm.save()



