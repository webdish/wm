from django import template
from django import forms
from core.models import Products
from django.db.models import Q
import re

register = template.Library()

# товары категорий
@register.inclusion_tag('templatetags/catalog/products_category.html')
def products_category(id):

    objects = Products.objects.filter(category__id=id)
    return { 'objects': objects }


@register.filter(name='test')
def test(value):
    return str(value)