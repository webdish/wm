from django.conf.urls import url
from . import views
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = [

    url(r'^$', views.index, name='index'),
    url(r'^transaction/create/(?P<value>[0-9]+)/$', views.transaction_create, name='transaction_create'),
    url(r'^transaction/change/(?P<transaction>[0-9]+)/(?P<value>[0-9]+)/$', views.transaction_change, name='transaction_change'),
    url(r'^transaction/count_1money/(?P<transaction>[0-9]+)/$', views.count_1money, name='count_1money'),
    url(r'^transaction/count_2money/(?P<transaction>[0-9]+)/$', views.count_2money, name='count_2money'),
    url(r'^transaction/count_5money/(?P<transaction>[0-9]+)/$', views.count_5money, name='count_5money'),
    url(r'^transaction/count_10money/(?P<transaction>[0-9]+)/$', views.count_10money, name='count_10money'),

    url(r'^wm/wallet10/$', views.wm_10money, name='wm_10money'),
    url(r'^wm/wallet5/$', views.wm_5money, name='wm_5money'),
    url(r'^wm/wallet2/$', views.wm_2money, name='wm_2money'),
    url(r'^wm/wallet1/$', views.wm_1money, name='wm_1money'),

    url(r'^pay/(?P<transaction>[0-9]+)/(?P<sdacha_1money>[0-9]+)/(?P<sdacha_2money>[0-9]+)/(?P<sdacha_5money>[0-9]+)/(?P<sdacha_10money>[0-9]+)/$', views.get_sdacha, name='get_sdacha'),

    url(r'^ammount_good/change/(?P<transaction>[0-9]+)/(?P<dataid>[0-9]+)/$', views.buy_good, name='buy_good'),

    #url(r'^opganizations/select_slug/(?P<slug>[\-\w]+)/(?P<slug_now>[\-\w]+)$', views.opganizations_select_slug, name='opganizations_select_slug'),

]