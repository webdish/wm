import json
import random, string
import time
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.files import File
from django.core.urlresolvers import reverse
from django.db import models
from django.http import HttpResponse, HttpResponseForbidden, QueryDict
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from math import ceil
from urllib.request import urlretrieve, urlopen
from django.views.decorators.csrf import csrf_exempt
from datetime import timedelta
from django.db.models import Count, Min, Sum, Avg
from django.db.models import Q
from django.contrib import auth
from django.shortcuts import render_to_response
from .models import *
from django.core import serializers
from django.http import Http404
from django.http import JsonResponse


#  главная страница
def index(request):

    categories = Category.objects.filter(is_visible=True)

    context = dict(
        categories= categories
    )
    return render(request, 'layout.html', context)


# клик на монету, создаем транзакцию если она не создана
def transaction_create(request, value):

    object = Transaction.objects.create()
    if int(value) == 1:
        object.rub1=1
    elif int(value) == 2:
        object.rub2=1
    elif int(value) == 5:
        object.rub5=1
    elif int(value) == 10:
        object.rub10=1
    object.save()

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    if object:
        response.write(object.id)
    else:
        response.write('')
    return response


def transaction_change(request, transaction, value):

    object = Transaction.objects.get(id=transaction)
    wm = WalletSystem.objects.first()
    if int(value) == 1:
        object.rub1=int(object.rub1)+1
        wm.rub1=int(wm.rub1)+1
    elif int(value) == 2:
        object.rub2=int(object.rub2)+1
        wm.rub2=int(wm.rub2)+1
    elif int(value) == 5:
        object.rub5=int(object.rub5)+1
        wm.rub5=int(wm.rub5)+1
    elif int(value) == 10:
        object.rub10=int(object.rub10)+1
        wm.rub10=int(wm.rub10)+1
    object.save()
    wm.save()

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    if object:
        response.write(object.total)
    else:
        response.write('')
    return response


# сколько в кошельке клиента монет 1 руб
def count_1money(request, transaction):

    object = Transaction.objects.get(id=transaction)
    value = int(object.rub1)

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(value)
    return response


# сколько в кошельке клиента монет 2 руб
def count_2money(request, transaction):

    object = Transaction.objects.get(id=transaction)
    value = int(object.rub2)

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(value)
    return response


# сколько в кошельке клиента монет 5 руб
def count_5money(request, transaction):

    object = Transaction.objects.get(id=transaction)
    value = int(object.rub5)

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(value)
    return response


# сколько в кошельке клиента монет 10 руб
def count_10money(request, transaction):

    object = Transaction.objects.get(id=transaction)
    value = int(object.rub10)

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(value)
    return response


#  наличие монет 10 в WM кошельке
def wm_10money(request):

    wm10 = WalletSystem.objects.first()
    wm10 = wm10.rub10

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(wm10)
    return response


#  наличие монет 5 в WM кошельке
def wm_5money(request):

    wm5 = WalletSystem.objects.first()
    wm5 = wm5.rub5

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(wm5)
    return response


#  наличие монет 2 в WM кошельке
def wm_2money(request):

    wm2 = WalletSystem.objects.first()
    wm2 = wm2.rub2

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(wm2)
    return response


#  наличие монет 1 в WM кошельке
def wm_1money(request):

    wm1 = WalletSystem.objects.first()
    wm1 = wm1.rub1

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(wm1)
    return response


# сдача денег
# вычитаем количество монет из WM кошелька и кошелька клиента
def get_sdacha(request, transaction, sdacha_1money, sdacha_2money, sdacha_5money, sdacha_10money):

    wm = WalletSystem.objects.first()
    wm.rub1 = wm.rub1 - int(sdacha_1money)
    wm.rub2 = wm.rub2 - int(sdacha_2money)
    wm.rub5 = wm.rub5 - int(sdacha_2money)
    wm.rub10 = wm.rub10 - int(sdacha_10money)
    wm.save()

    object = Transaction.objects.get(id=transaction)
    object.rub1 = 0
    object.rub2 = 0
    object.rub5 = 0
    object.rub10 = 0
    object.save()

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(object)
    return response


def buy_good(request, transaction, dataid):

    good = Products.objects.get(id=dataid)
    good.quantity = int(good.quantity)-1
    good.save()

    object = Transaction.objects.get(id=transaction)
    ost10 = good.price - object.rub10*10
    if ost10 == 0:
        object.rub10 = 0
    elif ost10 > 0:
        object.rub10 = ost10
    elif ost10 < 0:
        pass

    object.total = object.total-good.price
    object.save()

    response = HttpResponse("", content_type="text/html; charset=utf-8")
    response['Access-Control-Allow-Origin'] = '*'
    response.write(object.total)
    return response