from decimal import Decimal
from datetime import datetime
from django.db import models
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
import uuid


def image_path(instance, filename):
    now = datetime.today()
    filename_parts = str(filename).split('.')
    if len(filename_parts) < 2:
        filename = uuid.uuid4()
    else:
        filename = '%s.%s' % (uuid.uuid4(), filename_parts[len(filename_parts) - 1])
    return "files/%s/%s/%s/%s/%s" % (now.year, now.month, now.day, now.hour, filename)


# категории товара
class Category(models.Model):

    name = models.CharField('Название', max_length=100)
    parent = models.ForeignKey('self', verbose_name='Родитель', blank=True, null=True, related_name='children')
    image = models.ImageField('Изображение', upload_to=image_path, max_length=250, blank=True, null=True)
    is_visible = models.BooleanField(u'Публиковать', default=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Category, self).save()

    #def get_absolute_url(self):
    #    return reverse('category_detail', args=[self.slug])

    class Meta:
        verbose_name = 'категорию товара'
        verbose_name_plural = 'Категории товара'
        app_label = 'core'

    def __str__(self):
        return self.name


# продукты
class Products(models.Model):

    name = models.CharField('Название', max_length=1000)
    category = models.ForeignKey(Category, verbose_name='Категория товара', null=True, blank=True)
    price = models.DecimalField('Цена без учета скидок', max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.IntegerField('Количество', null=True, blank=True)
    image = models.ImageField('Изображение', upload_to=image_path, max_length=250, blank=True, null=True)
    is_visible = models.BooleanField('Публиковать', default=True)

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'Каталог товаров'
        app_label = 'core'

    def __str__(self):
        return self.name


class WalletSystem(models.Model):

    rub1 = models.IntegerField('Количество монет 1руб', default=0)
    rub2 = models.IntegerField('Количество монет 2руб', default=0)
    rub5 = models.IntegerField('Количество монет 5руб', default=0)
    rub10 = models.IntegerField('Количество монет 10руб', default=0)
    total = models.DecimalField('Общая сумма', max_digits=10, decimal_places=2, default=0)

    class Meta:
        verbose_name = 'обновление суммы'
        verbose_name_plural = 'Кошелек системы'
        app_label = 'core'

    def save(self, *args, **kwargs):
        total = 0
        rub1 = self.rub1
        rub2 = self.rub2*2
        rub5 = self.rub5*5
        rub10 = self.rub10*10
        total = rub1+rub2+rub5+rub10
        self.total = total
        super(WalletSystem, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.total)


class Transaction(models.Model):

    rub1 = models.IntegerField('Количество монет 1руб', default=0, blank=True, null=True)
    rub2 = models.IntegerField('Количество монет 2руб', default=0, blank=True, null=True)
    rub5 = models.IntegerField('Количество монет 5руб', default=0, blank=True, null=True)
    rub10 = models.IntegerField('Количество монет 10руб', default=0, blank=True, null=True)
    total = models.DecimalField('Общая сумма', max_digits=10, decimal_places=2, default=0, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создан', null=True, blank=False)

    class Meta:
        verbose_name = 'транзакцию'
        verbose_name_plural = 'Финансовые транзакции'
        app_label = 'core'

    def save(self, *args, **kwargs):
        total = 0
        rub1 = self.rub1
        rub2 = self.rub2
        rub5 = self.rub5
        rub10 = self.rub10
        total = rub1+rub2*2+rub5*5+rub10*10
        self.total = total
        super(Transaction, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.total)
