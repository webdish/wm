jQuery(document).ready(function(){	

    //$.cookie('transaction', 0);

    var transaction = $.cookie('transaction');
    var total_summ = $.cookie('total_summ');
    var my_money_1rub =  $.cookie('my_money_1rub');
    var my_money_2rub =  $.cookie('my_money_2rub');
    var my_money_5rub =  $.cookie('my_money_5rub');
    var my_money_10rub =  $.cookie('my_money_10rub');

    var sdacha_1money =  $.cookie('sdacha_1money');
    var sdacha_2money =  $.cookie('sdacha_2money');
    var sdacha_5money =  $.cookie('sdacha_5money');
    var sdacha_10money =  $.cookie('sdacha_10money');
    var total_sdacha =  $.cookie('total_sdacha');

    var wm_1money =  $.cookie('wm_1money');
    var wm_2money =  $.cookie('wm_2money');
    var wm_5money =  $.cookie('wm_5money');
    var wm_10money =  $.cookie('wm_10money');

    function wm_wallet(){
        var url = '/wm/wallet10/';
        $.ajax({
            url: url,
            type: 'get',
            contentType: 'text/plain',
            Origin: 'http://javascript.ru',
            success: function(response){
                $.cookie("wm_10money", response, { expires: 365});
                $('#wm_10money').text(response);
            },
            failure: function(response) {
                alert('Got an error dude');
            }
        });

        var url = '/wm/wallet5/';
        $.ajax({
            url: url,
            type: 'get',
            contentType: 'text/plain',
            Origin: 'http://javascript.ru',
            success: function(response){
                $.cookie("wm_5money", response, { expires: 365});
                $('#wm_5money').text(response);
            },
            failure: function(response) {
                alert('Got an error dude');
            }
        });

        var url = '/wm/wallet2/';
        $.ajax({
            url: url,
            type: 'get',
            contentType: 'text/plain',
            Origin: 'http://javascript.ru',
            success: function(response){
                $.cookie("wm_2money", response, { expires: 365});
                $('#wm_2money').text(response);
            },
            failure: function(response) {
                alert('Got an error dude');
            }
        });

        var url = '/wm/wallet1/';
        $.ajax({
            url: url,
            type: 'get',
            contentType: 'text/plain',
            Origin: 'http://javascript.ru',
            success: function(response){
                $.cookie("wm_1money", response, { expires: 365});
                $('#wm_1money').text(response);
            },
            failure: function(response) {
                alert('Got an error dude');
            }
        });
    }

    function dont_want_buy(){
        $('#sdacha_1money').text(0);
        $('#sdacha_2money').text(0);
        $('#sdacha_5money').text(0);
        $('#sdacha_10money').text(0);

        $('#my_money_1rub').text(0);
        $('#my_money_2rub').text(0);
        $('#my_money_5rub').text(0);
        $('#my_money_10rub').text(0);

        $('.my_money_total').text(0);
        $('#hh').text("Станьте покупателем!");

        $.cookie('transaction', 0);
        $.cookie('total_summ', 0);
        $.cookie('my_money_1rub', 0);
        $.cookie('my_money_2rub', 0);
        $.cookie('my_money_5rub', 0);
        $.cookie('my_money_10rub', 0);

        $.cookie('sdacha_1money', 0);
        $.cookie('sdacha_2money', 0);
        $.cookie('sdacha_5money', 0);
        $.cookie('sdacha_10money', 0);
        $.cookie('total_sdacha', 0);

        $(".sdacha, .wallet_wm, .dont_want_buy").hide();
    }


    function sdacha_money(transaction, total_summ){
        if ($.cookie('transaction') > 0){

            // Оработка монет на сдачу 10руб
            need_money10 = parseInt(total_summ/10);
            have_money10 = wm_10money;
            if (have_money10 >=need_money10){
                $.cookie('sdacha_10money', need_money10);
            }else{
                $.cookie('sdacha_10money', have_money10);
            }
            sdacha_10money =  $.cookie('sdacha_10money');
            $('#sdacha_10money').text(sdacha_10money);

            ost = total_summ-sdacha_10money*10;

            if (ost > 0){
                // Оработка монет на сдачу 5руб
                // остаток суммы
                need_money5 = parseInt(ost/5);
                have_money5 = wm_5money;
                if (have_money5 >=need_money5){
                    $.cookie('sdacha_5money', need_money5);
                }else{
                    $.cookie('sdacha_5money', have_money5);
                }
                sdacha_5money =  $.cookie('sdacha_5money');
                $('#sdacha_5money').text(sdacha_5money);

                // Оработка монет на сдачу 2руб
                // остаток суммы
                ost2 = total_summ-sdacha_10money*10-sdacha_5money*5;
                need_money2 = parseInt(ost2/2);
                have_money2 = wm_2money;
                if (have_money2 >=need_money2){
                    $.cookie('sdacha_2money', need_money2);
                }else{
                    $.cookie('sdacha_2money', have_money2);
                }
                sdacha_2money =  $.cookie('sdacha_2money');
                $('#sdacha_2money').text(sdacha_2money);

                // Оработка монет на сдачу 1руб
                // остаток суммы
                ost1 = total_summ-sdacha_10money*10-sdacha_5money*5-sdacha_2money*2;
                need_money1 = parseInt(ost1);
                have_money1 = wm_1money;
                if (have_money1 >=need_money1){
                    $.cookie('sdacha_1money', need_money1);
                }else{
                    $.cookie('sdacha_1money', have_money1);
                }
                sdacha_1money =  $.cookie('sdacha_1money');
                $('#sdacha_1money').text(sdacha_1money);
            }

        }
    }

    function count_money(transaction, money_value){
          // количество монет 1руб
          if (($.cookie('transaction') > 0)&&(money_value==1)){
            var url = '/transaction/count_1money/'+transaction+'/';
            $.ajax({
                url: url,
                type: 'get',
                contentType: 'text/plain',
                Origin: 'http://javascript.ru',
                success: function(response){
                    $.cookie("my_money_1rub", response, { expires: 365});
                    $('#my_money_1rub').text(response);
                },
                failure: function(response) {
                    alert('Got an error dude');
                }
            });
          }

          // количество монет 2руб
          if (($.cookie('transaction') > 0)&&(money_value==2)){
            var url = '/transaction/count_2money/'+transaction+'/';
            $.ajax({
                url: url,
                type: 'get',
                contentType: 'text/plain',
                Origin: 'http://javascript.ru',
                success: function(response){
                    $.cookie("my_money_2rub", response, { expires: 365});
                    $('#my_money_2rub').text(response);
                },
                failure: function(response) {
                    alert('Got an error dude');
                }
            });
          }

          // количество монет 5руб
          if (($.cookie('transaction') > 0)&&(money_value==5)){
            var url = '/transaction/count_5money/'+transaction+'/';
            $.ajax({
                url: url,
                type: 'get',
                contentType: 'text/plain',
                Origin: 'http://javascript.ru',
                success: function(response){
                    $.cookie("my_money_5rub", response, { expires: 365});
                    $('#my_money_5rub').text(response);
                },
                failure: function(response) {
                    alert('Got an error dude');
                }
            });
          }

          // количество монет 10руб
          if (($.cookie('transaction') > 0)&&(money_value==10)){
            var url = '/transaction/count_10money/'+transaction+'/';
            $.ajax({
                url: url,
                type: 'get',
                contentType: 'text/plain',
                Origin: 'http://javascript.ru',
                success: function(response){
                    $.cookie("my_money_10rub", response, { expires: 365});
                    $('#my_money_10rub').text(response);
                },
                failure: function(response) {
                    alert('Got an error dude');
                }
            });
          }
    }

    if (total_summ > 0) {
        $('.wallet .my_money_total').text(total_summ);
        $('#hh').text("Уважаемый покупатель!");
        $(".sdacha, .wallet_wm, .dont_want_buy").show();
    }
    else{
        $('#hh').text("Станьте покупателем!");
    }

    if (my_money_1rub > 0) {
        $('#my_money_1rub').text(my_money_1rub);
    }

    if (my_money_2rub > 0) {
        $('#my_money_2rub').text(my_money_2rub);
    }

    if (my_money_5rub > 0) {
        $('#my_money_5rub').text(my_money_5rub);
    }

    if (my_money_10rub > 0){
        $('#my_money_10rub').text(my_money_10rub);
    }

    if (sdacha_1money > 0){
        $('#sdacha_1money').text(sdacha_1money);
    }

    if (sdacha_2money > 0){
        $('#sdacha_2money').text(sdacha_2money);
    }

    if (sdacha_5money > 0){
        $('#sdacha_5money').text(sdacha_5money);
    }

    if (sdacha_10money > 0){
        $('#sdacha_10money').text(sdacha_10money);
    }

    // монеты WM кошелька
    if (wm_1money > 0){
        $('#wm_1money').text(wm_1money);
    }
    if (wm_2money > 0){
        $('#wm_2money').text(wm_2money);
    }
    if (wm_5money > 0){
        $('#wm_5money').text(wm_5money);
    }
    if (wm_10money > 0){
        $('#wm_10money').text(wm_10money);
    }

    // если у нас есть транзакция и уже несены монеты, обновляем на странице интерфейса внесенную сумму


    $("a.put_money").click(
      function () {
          var money_value = $(this).attr("data-value");
          if ((transaction == 0)||(!transaction)){
               var url = '/transaction/create/'+money_value+'/';
               $.ajax({
                   url: url,
                   type: 'get',
                   contentType: 'text/plain',
                   Origin: 'http://javascript.ru',
                   success: function(response){
                    $.cookie("transaction", response, { expires: 365});
                    $.cookie("total_summ", money_value, { expires: 365});
                    $('.wallet .my_money_total').text(money_value);
                    $('#hh').text("Уважаемый покупатель!");
                    $(".sdacha, .wallet_wm, .dont_want_buy").show();
                    count_money(response, money_value);
                    sdacha_money(response, money_value);
                    wm_wallet();
                   },
                   failure: function(response) {
                    alert('Got an error dude');
                   }
               });
          }
          else{
               var url = '/transaction/change/'+transaction+'/'+money_value+'/';
               $.ajax({
                   url: url,
                   type: 'get',
                   contentType: 'text/plain',
                   Origin: 'http://javascript.ru',
                   success: function(response){
                     $.cookie("total_summ", response, { expires: 365});
                     $('.wallet .my_money_total').text(response);
                     count_money(transaction, money_value);
                     sdacha_money(transaction, response);
                     wm_wallet();
                   },
                   failure: function(response) {
                    alert('Got an error dude');
                   }
               });
          }


      }
    );

    // запрос на выдачу денег
    // вычитаем монеты из WM кошелька, пересчитываем оставшиеся средства в WM кошельке и кошельке клиента
    // берем текущую транзакцию
    $(".sdacha a.get_sdacha").click(
      function () {
        if (transaction > 0){
            sdacha_1money =  $.cookie('sdacha_1money');
            sdacha_2money =  $.cookie('sdacha_2money');
            sdacha_5money =  $.cookie('sdacha_5money');
            sdacha_10money =  $.cookie('sdacha_10money');
            var url = '/pay/'+transaction+'/'+sdacha_1money+'/'+sdacha_2money+'/'+sdacha_5money+'/'+sdacha_10money+'/';
            $.ajax({
                url: url,
                type: 'get',
                contentType: 'text/plain',
                Origin: 'http://javascript.ru',
                success: function(response){
                    dont_want_buy();
                },
                failure: function(response) {
                    alert('Got an error dude');
                }
            });
        }
      }
    );

    $(".dont_want_buy a").click(
      function () {
        dont_want_buy();
      }
    );

    // проверка наличия нужной суммы на счету клиента для покупки определенного товара
    // проверка наличия товара перед покупкой
    $(".goods .category .row a.buy_good").click(
      function () {
          total_summ = $.cookie('total_summ');
          if (total_summ>0){
            dataprice = parseFloat($(this).attr("data-price"));
            if (parseFloat(total_summ) >= dataprice){
                // проверка наличия товара
                ammount = $(this).parent().find(".quantity_good").text();
                if (ammount){
                    ammount = parseInt(ammount);
                    if (ammount > 0){
                        // списываем количество товара
                        ammount = ammount-1;
                        $(this).parent().find(".quantity_good").text(ammount);
                        dataid = parseFloat($(this).attr("data-id"));
                        url = '/ammount_good/change/'+transaction+'/'+dataid+'/';
                        $.ajax({
                           url: url,
                           type: 'get',
                           contentType: 'text/plain',
                           Origin: 'http://javascript.ru',
                           success: function(response){
                             $.cookie("total_summ", response, { expires: 365});
                             $('.wallet .my_money_total').text(response);
                             count_money(transaction, response);
                             sdacha_money(transaction, response);
                             wm_wallet();
                             location.reload();
                           },
                           failure: function(response) {
                            alert('Got an error dude');
                           }
                        });
                    }
                    else{
                        alert('Извините, товара нет в наличие!!!');
                    }
                }
            }
            else{
                alert('Недостатоно средств для покупки этого товара. Пополните баланс.')
            }
          }
          else{
              alert('В Вашем кошельке недостаточно средств!');
          }
      }
    );

});
